<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="software"><title>Oprogramowanie dostępne w Debianie</title>
<section id="apps"><title>Jakiego typu aplikacje i programy narzędziowe oferuje Debian GNU/Linux?</title>
<para>
Tak jak większość dystrybucji Linuksa, Debian GNU/Linux oferuje:
</para>
<itemizedlist>
<listitem>
<para>
główne publikacje GNU służące do programowania, obsługi plików i
obróbki tekstu, takie jak gcc, g++, make, texinfo, Emacs, powłoka Bash i
wiele ulepszonych narzędzi systemu Unix,
</para>
</listitem>
<listitem>
<para>
Perl, Python, Tcl/Tk i różne powiązane z nimi programy, moduły i
biblioteki,
</para>
</listitem>
<listitem>
<para>
TeX (LaTeX) i Lyx, dvips, Ghostscript,
</para>
</listitem>
<listitem>
<para>
X Window System, który udostępnia sieciowy graficzny interfejs użytkownika
wraz z niezliczoną ilością aplikacji napisanych dla środowiska X takich jak
GNOME,
</para>
</listitem>
<listitem>
<para>
pełen wachlarz aplikacji sieciowych: serwery protokołów internetowych takich
jak HTTP (WWW), FTP, NNTP (news), SMTP i POP (poczta), serwery nazw; dostępne
są przeglądarki internetowe i narzędzia do tworzenia stron WWW.
</para>
</listitem>
</itemizedlist>
<para>
Debian obejmuje ponad 42304 pakietów, począwszy od serwerów wiadomości i
przeglądarek poprzez programy obsługujące dźwięk, programy faksujące,
bazy danych i arkusze kalkulacyjne, programy do obróbki obrazu, komunikacji,
sieci, narzędzia pocztowe, serwery sieciowe, a nawet programy obsługujące
ham-radio.
</para>
<para>
Część programów jest dostępna jako pakiety Debiana, choć - z powodu
restrykcji licencyjnych - nie są formalną częścią dystrybucji.  Ich
aktualna ilość to 696.
</para>
</section>

<section id="softwareauthors"><title>Kto to wszystko napisał?</title>
<para>
<emphasis>Autorzy</emphasis> każdego z programów w pakiecie są wymienieni w
pliku <literal>/usr/doc/PAKIET/copyright</literal>, gdzie PAKIET należy
zastąpić nazwą pakietu.
</para>
<para>
<emphasis>Koordynatorzy</emphasis>, którzy z tego oprogramowania utworzyli
pakiet dla systemu Debian GNU/Linux są wymienieni w pliku kontrolnym (zobacz
<xref linkend="controlfile"/>), znajdującym się w każdym pakiecie.
</para>
</section>

<section id="pkglist"><title>W jaki sposób mogę pobrać aktualną listę programów, które istnieją w pakietach Debiana?</title>
<para>
Kompletna lista jest dostępna w dwóch częściach:
</para>
<variablelist>
<varlistentry>
<term>lista pakietów, które mogą być wszędzie dystrybuowane</term>
<listitem>
<para>
z dowolnego <ulink url="http://www.debian.org/distrib/ftplist/">serwera
lustrzanego Debiana</ulink>, w pliku <literal>indices/Maintainers</literal>.
Ten plik zawiera nazwy pakietów oraz nazwiska i adresy ich koordynatorów.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>lista pakietów, które nie mogą być dystrybuowane poza Stany Zjednoczone</term>
<listitem>
<para>
z dowolnego <ulink url="http://www.debian.org/mirror/list-non-US">serwera
lustrzanego Debiana non-US</ulink>, w pliku
<literal>indices-non-US/Maintainers</literal>.  Ten plik zawiera nazwy
pakietów oraz nazwiska i adresy ich koordynatorów.
</para>
</listitem>
</varlistentry>
</variablelist>
<para>
Strona <ulink url="http://packages.debian.org/">Dostęp do pakietów Debiana
przez WWW</ulink> w wygodny sposób przedstawia informację o pakietach
podzielonych na około dwadzieścia "sekcji" archiwum Debiana.
</para>
</section>

<section id="missing"><title>Czego brakuje?</title>
<para>
Lista programów, które wciąż czekają na umieszczenie w pakietach Debiana
to <ulink url="http://www.debian.org/devel/wnpp/">Work-Needing and Prospective
Packages list</ulink>.
</para>
<para>
Więcej informacji o dodawaniu brakujących rzeczy znajdziesz <xref
linkend="contrib"/>.
</para>
</section>

<section id="no-devs"><title>Dlaczego podczas kompilowania programów dostaję komunikat "ld: cannot find -lfoo"? Dlaczego nie ma libfoo.so w pakiecie bibliotek Debiana?</title>
<para>
Polityka Debiana wymaga, by taki link symboliczny (np.  do libfoo.so.x.y.z)
był umieszczany w oddzielnym pakiecie dla deweloperów.  Takie pakiety zwykle
nazywają się libfoo-dev lub libfooX-dev (zakładając, że pakiet z
biblioteką nazywa się libfooX, gdzie X jest numerem).
</para>
</section>

<section id="java"><title>Czy Debian obsługuje Javę?</title>
<para>
Oficjalny Java Development kit z Sun Microsystems nie jest oprogramowaniem
wolnodostępnym, i nie może znajdować się w dystrybucji Debiana.  Jednakże
zarówno JDK jak i kilka <emphasis>wolnych</emphasis> implementacji technologii
Java są dostępne w pakietach Debiana.  Możesz pisać, poprawiać i
uruchamiać w Debianie programy napisane w Javie.
</para>
<para>
Uruchomienie apletu Javy wymaga przeglądarki internetowej z możliwością
obsługi technologii Java.  Kilka przeglądarek dostępnych w Debianie, takie
jak Mozilla czy Konqueror obsługuje wtyczkę Java, co pozwala na uruchamianie
w nich apletów Java.  Netscape Navigator, choć nie jest wolnym
oprogramowaniem, również jest dostępny jako pakiet Debiana i potrafi
uruchamiać aplety Javy.
</para>
<para>
Sięgnij do <ulink
url="http://www.debian.org/doc/manuals/debian-java-faq/">Debian Java
FAQ</ulink> po więcej informacji.
</para>
</section>

<section id="isitdebian"><title>Jak mogę sprawdzić, czy używam Debiana i której jego wersji?</title>
<para>
W celu upewnienia się, że twój system został zainstalowany z prawdziwych
dysków dystrybucyjnych Debiana sprawdź, czy istnieje plik
<literal>/etc/debian_version</literal>, który zawiera tylko jedną linię z
numerem wersji takim, jaki wynika z pakietu <literal>base-files</literal>.
</para>
<para>
Obecność programu <literal>dpkg</literal> świadczy o tym, że możesz
instalować pakiety Debiana w swoim systemie, ale program ten został
zaadoptowany przez wiele innych systemów operacyjnych i architektur, więc
przestał to być wiarygodny sposób upewniania się, że twój system to
Debian GNU/Linux.
</para>
<para>
Musisz pamiętać, że Debian składa się z wielu części, z których niemal
każda może być uaktualniana niezależnie.  Każde wydanie Debiana zawiera
starannie zdefiniowaną i niezmienną zawartość.  Uaktualnienia są dostępne
oddzielnie.  Jednowierszowy opis statusu instalacji pakietu
<literal>foo</literal> jest dostępny po wydaniu polecenia <literal>dpkg --list
foo</literal>.  By zobaczyć wersję wszystkich aktualnie zainstalowanych
pakietów, uruchom:
</para>
<screen>
dpkg -l
</screen>
<para>
Bardziej szczegółowa informacja dostępna jest przez:
</para>
<screen>
dpkg --status foo
</screen>
</section>

<section id="nonenglish"><title>Jak Debian obsługuje języki inne niż angielski?</title>
<itemizedlist>
<listitem>
<para>
Debian GNU/Linux jest rozpowszechniany z mapami znaków dla blisko dwóch
tuzinów klawiatur wraz z narzędziami (w pakiecie <literal>kbd</literal>) do
instalowania, przeglądania i modyfikowania map.
</para>
<para>
Podczas instalacji systemu użytkownik jest proszony o określenie klawiatury
</para>
</listitem>
<listitem>
<para>
Znaczna większość programów w pakietach obsługuje wprowadzanie znaków
innych niż US-ASCII używanych w innych językach (np.  ISO-8859-1 czy
ISO-8859-2); niektóre programy obsługują wielobajtowe języki, takie jak
japoński czy chiński.
</para>
</listitem>
<listitem>
<para>
Obecnie dostępna jest dokumentacja w języku niemieckim, hiszpańskim,
węgierskim, fińskim, włoskim, japońskim, koreańskim i polskim poprzez
pakiet <literal>manpages-JĘZYK</literal> (gdzie JĘZYK jest dwuliterowym kodem
kraju).  Aby korzystać z dokumentacji w innym języku niż angielski, należy
ustawić odpowiednią wartość zmiennej LC_MESSAGES.
</para>
<para>
Na przykład dla polskich stron dokumentacji (tzw.  manuals), zmienna
LC_MESSAGES musi mieć wartość 'pl_PL'.  Spowoduje to, że program
<command>man</command> będzie starał się znaleźć w
<literal>/usr/share/man/pl/</literal> polską dokumentację.
</para>
</listitem>
</itemizedlist>
</section>

<section id="usexports"><title>Jak wyglądają regulacje prawne dotyczące amerykańskich przepisów eksportowych?</title>
<para>
Prawo amerykańskie zabrania eksportu zaawansowanych technologii obronnych,
takich jak np.  programy szyfrujące.  Dotyczy to m.in.  PGP czy SSH.
</para>
<para>
Aby uchronić przed ryzykiem postępowania wbrew prawu, niektóre pakiety
Debiana dostępne są tylko z serwera non-US <ulink
url="ftp://non-US.debian.org/debian-non-US/">ftp://non-US.debian.org/debian-non-US/</ulink>.
Jest także kilka serwerów lustrzanych poza Stanami Zjednoczonymi.  Pełna
lista znajduje się <ulink
url="ftp://non-US.debian.org/debian-non-US/README.non-US">tutaj</ulink>.
</para>
</section>

<section id="pine"><title>Gdzie podział się pine?</title>
<para>
Z powodu obostrzeń w licencji, znajduje się on w obszarze non-free.  Ponadto
licencja nie zezwala nawet na rozpowszechnianie zmodyfikowanych plików
binarnych, będziesz więc musiał samodzielnie go skompilować z plików
źródłowych i nakładek Debiana.
</para>
<para>
Nazwa pakietu ze źródłami to <systemitem role="package">pine</systemitem>.
Możesz użyć pakietu <systemitem role="package">pine-tracker</systemitem> by
uzyskać informację o koniecznościach uaktualnień.
</para>
<para>
Zauważ, że jest wiele darmowych zamienników zarówno pine jak i pico, takich
jak <systemitem role="package">mutt</systemitem> i <systemitem
role="package">nano</systemitem>, które dostępne są w głównej części
pakietów.
</para>
</section>

</chapter>

