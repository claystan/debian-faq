<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="basic-defs"><title>Definicje i przegląd</title>
<section id="whatisdebian"><title>Co to jest Debian GNU/Linux?</title>
<para>
Debian GNU/Linux jest szczególną <emphasis>dystrybucją</emphasis> systemu
operacyjnego Linux i licznych pakietów, które można na nim uruchomić.
</para>
<para>
W zasadzie, użytkownicy mogą pobrać z Internetu lub innych źródeł jądro
Linux, a następnie je samodzielnie skompilować.  Mogą dalej pobrać kody
źródłowe wielu aplikacji w ten sam sposób, skompilować je, a potem
zainstalować na swoim systemie.  W przypadku skomplikowanych programów, ten
proces może być nie tylko czasochłonny, ale też może powodować błędy.
Aby tego uniknąć, użytkownicy często wolą otrzymać system operacyjny i
pakiety z oprogramowaniem od jednego z dystrybutorów Linuksa.  To co
wyróżnia od siebie różne dystrybucje Linuksa to wybór oprogramowania,
protokołów, sposób tworzenia, instalacji i zarządzania pakietami wraz z
narzędziami za to odpowiedzialnymi.  Do tego dochodzi jeszcze dokumentacja i
inne usługi.
</para>
<para>
Debian GNU/Linux jest wynikiem prac i poświecenia wielu ochotników, którym
przyświecał cel stworzenia wysokiej jakości wolnego systemu operacyjnego
zawierającego pełną gamę aplikacji.  Idea wolnego systemu uniksopodobnego
zrodziła się wraz z projektem GNU, a wiele aplikacji, które czynią system
Debian GNU/Linux tak użytecznym, zostało opracowanych w ramach właśnie tego
projektu.
</para>
<para>
Dla Debiana wolność (z angielskiego ,,free'', co znaczy zarówno
,,wolność'' jak i ,,darmowość'') ma znaczenie zgodne z rozumieniem projektu
GNU (zobacz <ulink
url="http://www.debian.org/social_contract#guidelines/">Wytyczne Debiana
dotyczące Oprogramowania Wolnodostępowego</ulink>).  Kiedy mówimy o wolnym
oprogramowaniu, odnosimy się do wolności, nie do jego szybkości działania.
Wolne oprogramowanie oznacza, że masz wolną rękę do dystrybuowania kopii,
że otrzymałeś kody źródłowe, że możesz zmieniać te programy, lub
używać ich części w innych wolnych programach.
</para>
<para>
Projekt Debian został stworzony przez Iana Murdocka w 1993 roku i na początku
był sponsorowany przez projekt GNU Fundacji Wolnego Oprogramowania (FSF).
Dziś deweloperzy Debiana myślą o nim jak o bezpośrednim potomku projektu
GNU.
</para>
<para>
Debian GNU/Linux jest:
</para>
<itemizedlist>
<listitem>
<para>
<emphasis role="strong">szeroko rozbudowany</emphasis>: aktualna liczba
pakietów w Debianie wynosi 42551.  Użytkownicy mogą wybrać, które pakiety
chcą zainstalować.  Debian dostarcza narzędzie do tego celu.  Listę i opisy
aktualnie dostępnych pakietów dla Debiana możesz znaleźć na każdym z
<ulink url="http://www.debian.org/distrib/ftplist/">serwerów
lustrzanych</ulink> Debiana.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">wolny w użytkowaniu i redystrybucji</emphasis>: Nie ma
żadnego konsorcjum członkowskiego czy wymagań płatności do brania udziału
w jego rozprowadzaniu i rozwijaniu.  Wszystkie pakiety, które formalnie są
częścią systemu Debian GNU/Linux są wolne w redystrybucji, zazwyczaj na
warunkach licencji GNU GPL.
</para>
<para>
Archiwum FTP Debiana zawiera również około 696 pakietów (w archiwum
<literal>non-free</literal> i <literal>contrib</literal>), które są
rozprowadzane pod specyficznymi warunkami (warunki rozprowadzania zawarte są w
każdym pakiecie).
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">dynamiczny</emphasis>: Dzięki udziałowi 1033
ochotników stale wnoszących nowy i ulepszony kod, Debian rozwija się
gwałtownie.  Co kilka miesięcy planowane są nowe wydania, a archiwum FTP
aktualizowane jest codziennie.
</para>
</listitem>
</itemizedlist>
<para>
Mimo, że Debian GNU/Linux jest Wolnym Oprogramowaniem, jest on podstawą na
której mogą powstawać kolejne dystrybucje Linuksa.  Przez dostarczanie
niezawodnego, szeroko rozbudowanego systemu podstawowego, Debian dostarcza
system Linux użytkownikom ze zwiększoną kompatybilnością i pozwala
twórcom dystrybucji Linuksa skupić uwagę na rzeczach, które powodują, że
ich dystrybucja jest szczególna.  Zobacz <xref linkend="childistro"/> w celu
uzyskania większej ilości informacji.
</para>
</section>

<section id="linux"><title>OK, teraz już wiem co to jest Debian... a co to jest Linux?!</title>
<para>
Mówiąc krótko, Linux jest jądrem uniksopodobnego sytemu operacyjnego.
Pierwotnie był on przeznaczony dla maszyn 386 (i lepszych), a teraz jest on w
trakcie przenoszenia na inne architektury, włącznie z wieloprocesorowymi.
Linuksa napisał Linus Torvalds i wielu informatyków z całego świata.
</para>
<para>
Poza samym jądrem, system ,,Linux'' zazwyczaj posiada:
</para>
<itemizedlist>
<listitem>
<para>
system plików zgodny ze Standardem Hierarchii Linuksowego Systemu Plików
<ulink url="http://www.pathname.com/fhs/">http://www.pathname.com/fhs/</ulink>.
</para>
</listitem>
<listitem>
<para>
szeroki wybór narzędzi Uniksowych, z których wiele było stworzonych przez
projekt GNU oraz Fundację Wolnego Oprogramowania (FSF).
</para>
</listitem>
</itemizedlist>
<para>
Kombinacja jądra Linuksa, jego systemu plików, oprogramowania pochodzącego z
projektu GNU i FSF, oraz innego oprogramowania, jest tak zaprojektowana, aby
osiągnąć kompatybilność ze standardem POSIX (IEEE 1003.1); zobacz <xref
linkend="otherunices"/>.
</para>
<para>
W celu uzyskania większej ilości informacji na temat Linuksa, zobacz stronę
Michaela K.  Johnsona <ulink
url="ftp://ibiblio.org/pub/Linux/docs/HOWTO/INFO-SHEET/">Linux Information
Sheet</ulink> oraz <ulink
url="ftp://ibiblio.org/pub/Linux/docs/HOWTO/META-FAQ/">Meta-FAQ</ulink>.
</para>
</section>

<section id="hurd"><title>Co to jest ten ,,Hurd''?</title>
<para>
Hurd jest zespołem serwerów uruchamianych na mikrojądrze GNU Mach.  Razem
tworzą podstawę dla systemu operacyjnego GNU.
</para>
<para>
Obecnie Debian jest dostępny tylko dla Linuksa, ale razem z Debian GNU/Hurd
zaczęliśmy również oferować Hurda jako platformę programistyczną, serwer
oraz stację roboczą.  Jednak Debian GNU/Hurd nie został jeszcze oficjalnie
wydany i nie nastąpi to jeszcze przez pewien czas.
</para>
<para>
Proszę zobaczyć <ulink
url="http://www.gnu.org/software/hurd/">http://www.gnu.org/software/hurd/</ulink>
w celu uzyskania większej ilości informacji o GNU/Hurd, oraz <ulink
url="http://www.debian.org/ports/hurd/">http://www.debian.org/ports/hurd/</ulink>
aby uzyskać więcej informacji o Debian GNU/Hurd.
</para>
</section>

<section id="difference"><title>Jaka jest różnica pomiędzy systemem Debian GNU/Linux a innymi dystrybucjami Linuksa? Dlaczego powinienem wybrać Debiana zamiast innej dystrybucji?</title>
<para>
Oto kluczowe cechy, które odróżniają Debiana od innych dystrybucji Linuksa:
</para>
<variablelist>
<varlistentry>
<term>System zarządzania pakietów Debiana:</term>
<listitem>
<para>
Cały system, lub jakakolwiek jego część, może być uaktualniany bez
ponownego formatowania, bez gubienia istniejącej już konfiguracji i (w
większości przypadków) bez ponownego uruchamiania komputera.  Większość
obecnie dostępnych dystrybucji Linuksa ma swego rodzaju system zarządzania
pakietów; system zarządzania pakietów Debiana jest unikalny oraz
szczególnie mocny.  (zobacz <xref linkend="pkg-basics"/>)
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>Otwarty rozwój:</term>
<listitem>
<para>
Podczas gdy inne dystrybucje Linuksa są rozwijane przez małe, indywidualne,
zamknięte grupy, lub komercyjnych sprzedawców, Debian jest jedyną
dystrybucją Linuksa, która jest rozwijana przez wiele osób za pośrednictwem
Internetu w tym samym duchu, co Linux oraz inne Wolne Oprogramowanie.
</para>
<para>
Ponad 1033 ochotników pracuje nad pakietami w liczbie ponad 42551 i
polepszaniem systemu Debian GNU/Linux.  Deweloperzy Debiana zasilają projekt
nie przez pisanie nowych programów (w większości przypadków), ale przez
tworzenie pakietów, według standardów projektu, zawierających istniejące
oprogramowanie, poprzez informowanie zewnętrznych deweloperów o nowych
błędach, oraz poprzez zapewnianie wsparcia użytkownikom.  Więcej informacji
dotyczącej tego jak zostać współpracownikiem znajduje się w <xref
linkend="contrib"/>.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>System śledzenia błędów (BTS):</term>
<listitem>
<para>
Fakt tak dużego geograficznego rozsiania deweloperów Debiana po całym
świecie wymaga do użycia specjalnych narzędzi i szybkiego informowania o
błędach i poprawkach, bez których rozwój systemu nie byłby tak dynamiczny.
Użytkownicy są zachęcani do informowania o zauważonych przez nich błędach
w stylu formalnym, po czym informacje o nich stają się dostępne w archiwach
WWW, lub poprzez pocztę elektroniczną.  Więcej o zarządzaniu logów
dotyczących błędów przeczytasz w tym FAQ w <xref linkend="buglogs"/>.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>Polityka Debiana:</term>
<listitem>
<para>
Debian ma rozbudowaną specyfikację własnych standardów jakości, politykę
Debiana.  Ten dokument definiuje jakość i standardy, które każdy pakiet
musi spełniać.
</para>
</listitem>
</varlistentry>
</variablelist>
<para>
Więcej informacji o tym znajdziesz na naszej stronie o <ulink
url="http://www.debian.org/intro/why_debian/">powodach dla których warto
wybrać Debiana</ulink>.
</para>
</section>

<section id="gnu"><title>Jak projekt Debian wygląda w porównaniu z projektem GNU Fundacji Wolnego Oprogramowania?</title>
<para>
System Debian jest tworzony w myśl idei Wolnego Oprogramowania, które było
najpierw bronione przez <ulink url="http://www.gnu.org/">Fundację Wolnego
Oprogramowania (FSF)</ulink> a w szczególności przez <ulink
url="http://www.stallman.org/">Richarda Stallmana</ulink>.  Potężne
narzędzia programistyczne, aplikacje i innego rodzaju oprogramowanie FSF, są
również kluczową częścią Debiana.
</para>
<para>
Projekt Debian jest oddzielną jednostką Fundacji Wolnego Oprogramowania
(FSF), jednakże komunikujemy się regularnie i razem współpracujemy nad
różnymi projektami.  Na wyraźną prośbę Fundacji Wolnego Oprogramowania
nazywamy nasz system ,,Debian GNU/Linux'' i jesteśmy szczęśliwi, że
spełniamy tę prośbę.
</para>
<para>
Stałym celem Fundacji Wolnego Oprogramowania (FSF) jest rozwijanie nowego
systemu operacyjnego zwanego GNU, bazującego na <ulink
url="http://www.gnu.org/software/hurd/">Hurdzie</ulink>.  Debian pracuje z FSF
nad tym systemem zwanym <ulink url="http://www.debian.org/ports/hurd/">Debian
GNU/Hurd</ulink>.
</para>
</section>

<section id="pronunciation"><title>Jak się wymawia słowo Debian i co ono oznacza?</title>
<para>
Słowo Debian wymawiamy: Deb'-i-en, z krótkim /e/ w części Deb i akcentem na
pierwszą sylabę.  Słowo Debian jest połączeniem imion Debry oraz Iana
Murdocka, który założył projekt.  (Słowniki dwuznacznie podają sposób
wymowy słowa Ian (!), ale Ian preferuje ee'-en.)
</para>
</section>

</chapter>

