<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="pkgtools"><title>Las herramientas de gestión de paquetes de Debian</title>
<section id="pkgprogs"><title>¿Qué programas tiene Debian para la gestión de los paquetes?</title>
<section id="dpkg"><title>dpkg</title>
<para>
Se trata del programa principal de gestión de paquetes.
<literal>dpkg</literal> puede ejecutarse con muchas opciones.  Algunas de las
más comunes son:
</para>
<itemizedlist>
<listitem>
<para>
Averiguar cuáles son las opciones existentes: <literal>dpkg --help</literal>.
</para>
</listitem>
<listitem>
<para>
Imprimir el fichero de control (y demás información) de un paquete
específico: <literal>dpkg --info fu_VVV-RRR.deb</literal>
</para>
</listitem>
<listitem>
<para>
Instalar un paquete (incluyendo el desempaquetado y la configuración) en el
sistema de ficheros del disco duro: <literal>dpkg --install
fu_VVV-RRR.deb</literal>.
</para>
</listitem>
<listitem>
<para>
Desempaquetar (pero no configurar) un paquete Debian en el sistema de ficheros
del disco duro: <literal>dpkg --unpack fu_VVV-RRR.deb</literal>.  Esta
operación <emphasis>no</emphasis> deja al paquete necesariamente operativo;
algunos ficheros, pueden necesitar algo más de configuración para funcionar
correctamente.  Esta orden borra cualquier versión del programa instalada
previamente, y ejecuta el script preinst asociado al paquete.
</para>
</listitem>
<listitem>
<para>
Configurar un paquete que ha sido previamente desempaquetado: <literal>dpkg
--configure fu</literal>.  Entre otras cosas, esta acción ejecuta el script
postinst asociado al paquete.  También actualiza los ficheros de
configuración del paquete especificados en <literal>conffiles</literal>.  Debe
notar que el argumento de la operación 'configurar' es el nombre del paquete
(p.ej.  fu), <emphasis>no</emphasis> el del archivo del paquete Debian (p.ej.
fu_VVV-RRR.deb).
</para>
</listitem>
<listitem>
<para>
Extraer un único fichero llamado digamos "blurf" (o un grupo de ficheros
llamados "blurf*" de un archivo de paquete Debian: <literal>dpkg --fsys-tarfile
fu_VVV-RRR.deb | tar -xf - blurf*</literal>
</para>
</listitem>
<listitem>
<para>
Borrar un paquete (pero no sus ficheros de configuración): <literal>dpkg
--remove fu</literal>.
</para>
</listitem>
<listitem>
<para>
Borrar un paquete (incluidos los ficheros de configuración): <literal>dpkg
--purge fu</literal>.
</para>
</listitem>
<listitem>
<para>
Listar el estado de los paquetes que contienen la cadena "fu*": <literal>dpkg
--list 'fu*'</literal>.
</para>
</listitem>
</itemizedlist>
</section>

</section>

<section id="dselect"><title>dselect</title>
<para>
Este programa es una interfaz basada en menús al sistema de gestión de
paquetes de Debian.  En particular es útil para primeras instalaciones y para
actualizaciones a gran escala.  <literal>dselect</literal> puede
</para>
<itemizedlist>
<listitem>
<para>
guiar al usuario al elegir qué paquetes instalar o borrar, asegurándose de
que ningún paquete esté en conflicto con otro, y que todos los paquetes
necesarios para que cada uno de los elegidos funcione estén adecuadamente
instalados.
</para>
</listitem>
<listitem>
<para>
avisar al usuario de inconsistencias e incompatibilidades en las elecciones
</para>
</listitem>
<listitem>
<para>
determinar el orden en el que los paquetes han de ser instalados.
</para>
</listitem>
<listitem>
<para>
realizar la instalación o borrado de forma automática y
</para>
</listitem>
<listitem>
<para>
guiar al usuario a través de cualquier proceso de configuración requerido
para cada paquete.
</para>
</listitem>
</itemizedlist>
<para>
<literal>dselect</literal> comienza presentando al usuario un menú de 7
entradas, cada una de las cuales corresponde a una acción concreta.  El
usuario puede seleccionar una acción utilizando las flechas del teclado que
mueven la barra iluminada y presionando INTRO para seleccionar la acción
iluminada.
</para>
<para>
Lo que el usuario ve a continuación depende de la acción elegida.  Si elige
una opción diferente de <literal>Access</literal> o <literal>Select</literal>,
<literal>dselect</literal> simplemente procederá a ejecutar la opción
seleccionada: p.ej., si el usuario eligió <literal>Remove</literal>, dselect
procederá a borrar todos los ficheros marcados para ser borrados la última
vez que el usuario ejecutó la opción <literal>Select</literal> para
elegirlos.
</para>
<para>
Las dos entradas <literal>Access</literal> y <literal>Select</literal> llevan a
menús adicionales.  En ambos casos, los menús se presentan en una pantalla
dividida en dos; la parte superior presenta una lista de opciones navegable,
mientras que la parte inferior presenta una explicación ("información") sobre
cada una de las opciones.
</para>
<para>
Una extensa ayuda en línea está disponible: Utilice la tecla '?'  para
acceder a la pantalla de ayuda y luego '.'  para sucesivamente ver cada una de
las páginas disponibles, una a una.
</para>
<para>
Algunos usuarios encuentran más fácil navegar por <literal>dselect</literal>
cuando usa colores en la salida por pantalla.  Para ver los colores, asegúrese
de que ha ejecutado: <literal>export TERM=linux</literal> antes de ejecutar
<literal>dselect</literal>.
</para>
<para>
El orden en el que las acciones se presentan en el menú inicial de
<literal>dselect</literal> refleja el orden en el que el usuario ejecutaría
normalmente <literal>dselect</literal> para instalar paquetes.  Sin embargo, un
usuario puede elegir cualquiera de las opciones tantas veces como sea necesario
(incluyendo no usar ninguna opción en absoluto, dependiendo de lo que quiera
hacer).
</para>
<itemizedlist>
<listitem>
<para>
Primeramente elija un "Método de Acceso" (<literal>Access Method</literal>).
Este será el método mediante el cual el usuario planea acceder a los paquetes
de Debian; p.ej., algunos usuarios pueden acceder a los paquetes de Debian
mediante un CD-ROM mientras que otros planean acceder a ellos mediante ftp
anónimo.  El "Médodo de Acceso" es almacenado al salir de
<literal>dselect</literal>, de forma que si no cambia, no será necesario
volver a usar esta opción más.
</para>
</listitem>
<listitem>
<para>
Luego, "Actualice" (<literal>Update</literal>) la lista de paquetes
disponibles.  Para ello, <literal>dselect</literal> lee el fichero
"Packages.gz" que debería estar incluido en el directorio superior de la
estructura que contiene los paquetes Debian que se van a instalar.  (Aunque si
no lo encuentra allí, <literal>dselect</literal> le permite la opción de
crearlo por usted.)
</para>
</listitem>
<listitem>
<para>
Elija (<literal>Select</literal>) para seleccionar los paquetes que desea
instalar en el sistema.
</para>
<para>
Tras seleccionar esta opción del menú, el sistema presenta al usuario una
pantalla de ayuda; se puede salir de la ayuda (de esta y de cualquiera)
presionando la barra espaciadora.  Lo mejor (si es la primera vez que utiliza
dselect) es que lea <emphasis>toda</emphasis> la ayuda, pero presionando '.'
repetidamente irá presentando todas las páginas una tras otra.  Una vez que
el usuario sale de la pantalla de ayuda, aparece el menú en dos secciones para
la elección de los paquetes a instalar (o borrar).  La parte superior es una
ventana relativamente estrecha que presenta una parte de la lista de los 42304
paquetes; la parte inferior de la pantalla es una ventana con "información"
sobre los paquetes o grupos de paquetes seleccionados en la parte superior.
</para>
<para>
Muchos usuarios noveles suelen mostrar confusión en relación a los siguientes
aspectos de la pantalla de la opción <literal>Select</literal>:
</para>
<itemizedlist>
<listitem>
<para>
"Marcar para borrado" de paquetes: Se puede especificar qué paquetes deben ser
borrados marcando el nombre del paquete o la etiqueta de un grupo de paquetes,
p.ej.  "Todos" (<literal>All</literal>) y presionando:
</para>
<itemizedlist>
<listitem>
<para>
la tecla '-'.  Esto borra la mayoría de los ficheros asociados con el paquete,
pero preserva los ficheros listados como ficheros de configuración (véase
<xref linkend="conffile"/>) y demás información de configuración.
</para>
</listitem>
<listitem>
<para>
la tecla '_'.  En este caso, se borra <emphasis>cualquier</emphasis> fichero
que pertenezca a este paquete.
</para>
</listitem>
</itemizedlist>
<para>
Observe que si "marca para borrar" "Todos los Paquetes" (<literal>All
Packages</literal>) su sistema quedará reducido a los paquetes base instalados
inicialmente.  Esto es problablemente lo que usted quería.
</para>
</listitem>
<listitem>
<para>
Si pone un paquete "en espera" (presionando `='): De forma efectiva, esto
indica a <literal>dselect</literal> que no actualice un paquete incluso en el
caso de que la versión presente en el sistema sea menos reciente que la
disponible en la fuente de la distribución que esté usando (la que fue
especificada cuando estableció el Método de Acceso con la opción
<literal>Access Method</literal>).  (La versión disponible en la distribución
viene en el fichero <literal>Packages.gz</literal> que se lee al activar la
opción de menú "Update")
</para>
<para>
Sacar un paquete del estado de espera (presionando ':'): Se trata de la opción
por defecto y significa que el paquete será actualizado siempre y cuando esté
disponible una versión más reciente.
</para>
</listitem>
<listitem>
<para>
Orden de presentación de los paquetes: El orden de presentación
predeterminado es por Prioridad; para cada valor de prioridad, los paquetes
aparecen ordenados según el directorio (alias sección) del archivo en el que
están almacenados.  De esa forma, algunos paquetes de (digamos) la sección A
pueden aparecer primero, seguidos de paquetes de la sección B, seguidos de
otros paquetes (de prioridad más baja) de la sección A.  Se puede cambiar el
orden de presentación utilizando la tecla 'o' que va ciclicamente, cambiando
de orden de presentación.
</para>
</listitem>
<listitem>
<para>
Significado de las etiquetas en la parte superior de la pantalla: Las etiquetas
de la parte superior pueden expandirse utilizando la tecla 'v' (del inglés
verbose, detallado).  Al hacer esto, gran parte del texto que originalmente
cabía en la pantalla se desplaza a la derecha.  Para poder verlo es necesario
usar la flecha derecha; para volver a la izquierda, utilice la flecha
izquierda.
</para>
</listitem>
<listitem>
<para>
Qué hacer en la pantalla de conflicto/dependencia: Si un usuario elige (tanto
para instalar como para borrar) un paquete, digamos <literal>fu.deb</literal>
que depende de o recomienda otro, digamos, <literal>blurf.deb</literal>,
entonces <literal>dselect</literal> presentará un extracto de la pantalla
principal de selección.  El proceso comienza presentando la ayuda a pantalla
completa, de la que se puede salir usando la barra espaciadora.  Después,
puede elegir los paquetes relacionados, aceptando las acciones sugeridas (de
instalar o no), o rechazándolas.  Para conseguir esto último, basta presionar
Mayúsculas-D; para volver a las sugerencias puede usar Mayúsculas-U.  En
cualquier caso, puede salvar su elección y volver al menú principal de
instalación mediante Mayúsculas-Q.
</para>
</listitem>
</itemizedlist>
</listitem>
<listitem>
<para>
Una vez de vuelta al menú principal, se puede proceder a la opción de
"Instalación" (<literal>Install</literal>) para desempaquetar y configurar los
paquetes elegidos.  Alternativamente, si lo que quiere es eliminar ficheros,
puede elegir la opción de "Borrado" (<literal>Remove</literal>).  Puede salir
del programa en cualquier momento mediante la opción <literal>Quit</literal>;
cualquier elección que haya hecho será preservada por
<literal>dselect</literal>.
</para>
</listitem>
</itemizedlist>
<section id="dpkg-deb"><title>dpkg-deb</title>
<para>
Este programa manipula un archivo de paquete Debian(<literal>.deb</literal>).
Algunos de sus usos comunes son:
</para>
<itemizedlist>
<listitem>
<para>
Averiguar cuáles son las opciones existentes: <literal>dpkg-deb
--help</literal>.
</para>
</listitem>
<listitem>
<para>
Determinar qué ficheros están contenidos en un archivo de paquete Debian:
<literal>dpkg-deb --contents fu_VVV-RRR.deb</literal>)
</para>
</listitem>
<listitem>
<para>
Extraer los ficheros contenidos en un archivo de paquete Debian dado en un
directorio especificado: <literal>dpkg-deb --extract fu_VVV-RRR.deb
tmp</literal> extraerá los ficheros de <literal>fu_VVV-RRR.deb</literal> en el
directorio <literal>tmp/</literal>.  Esta operación es conveniente para
examinar el contenido de un paquete en un directorio concreto, sin necesidad de
instalar el paquete en el sistema de ficheros raíz.
</para>
</listitem>
</itemizedlist>
<para>
Puede obtener más información en la página de manual de
<literal>dpkg-deb</literal>(1).
</para>
</section>

<section id="dpkgsplt"><title>dpkg-split</title>
<para>
Este programa divide un paquete grande en ficheros más pequeños (p.ej.  cara
a copiarlo mediante un grupo de disquetes) y puede usarse para unir de nuevo
los ficheros en uno solo.  Sólo se puede usar este programa en un sistema
Debian pues se vale de <literal>dpkg-deb</literal> para desglosar el archivo de
paquete Debian en sus componentes.  Así, por ejemplo, si quisiera dividir un
fichero .deb grande en N partes,
</para>
<itemizedlist>
<listitem>
<para>
Ejecute la orden <literal>dpkg-split --split fu.deb</literal>.  Esto producirá
N ficheros de aproximandamente 460 KBytes cada uno en el directorio actual.
</para>
</listitem>
<listitem>
<para>
Copie los N ficheros en disquetes.
</para>
</listitem>
<listitem>
<para>
Copie el contenido de los disquetes en el disco duro que haya elegido en la
otra máquina.
</para>
</listitem>
<listitem>
<para>
Una los ficheros parciales mediante <literal>dpkg-split --join "fu*"</literal>.
</para>
</listitem>
</itemizedlist>
</section>

<section id="s7.2.3"><title>Debian asegura ser capaz de actualizar programas que están siendo ejecutados; ¿Cómo se puede hacer esto?</title>
<para>
Debian GNU/Linux incluye un programa llamado
<literal>start-stop-daemon</literal> que es usado por los scripts de
instalación para arrancar demonios durante el proceso de arranque de la
máquina o para pararlos cuando cambia el nivel de ejecución del núcleo
(p.ej.  si se cambia de multi-usuario a usuario-único o a parada).  El
programa <literal>start-stop-daemon</literal> también se utiliza cuando se
instala un paquete nuevo que contiene un demonio, para parar demonios en
ejecución y rearrancarlos según sea necesario, p.ej.  cuando se instala un
paquete que contiene un script de configuración actualizado.
</para>
</section>

<section id="s7.2.4"><title>¿Cómo se puede comprobar qué paquetes han sido instalados en un sistema Debian?</title>
<para>
Para averiguar cuál es el estado de todos los paquetes instalados en un
sistema Debian, debe ejecutar la orden <literal>dpkg --list</literal>.  Esto
muestra un resumen de una línea por cada paquete, que contiene un símbolo de
estado de 2 letras, el nombre del paquete correspondiente, la versión que
está <emphasis>instalada</emphasis>, y una descripción muy breve del mismo.
</para>
<para>
Para averiguar el estado de los paquetes que encajan con un patrón que empiece
por "fu" puede ejecutar la orden <literal>dpkg --list 'fu*'</literal>
</para>
<para>
Para obtener información más detallada de un paquete en particular puede usar
la orden <literal>dpkg --status fu</literal>.
</para>
</section>

</section>

<section id="filesearch"><title>¿Cómo se puede averiguar qué paquete produjo un fichero en particular?</title>
<para>
Para indentificar el paquete que produjo un fichero llamado
<literal>fu</literal> puede usar cualquiera de las siguientes opciones:
</para>
<itemizedlist>
<listitem>
<para>
<literal>dpkg --search nombredefichero</literal>.
</para>
<para>
En este caso se busca el fichero <literal>nombredefichero</literal> entre los
paquetes instalados.  (Esto equivale (actualmente) a buscar todos los ficheros
de extensión <literal>.list</literal> contenidos en el directorio
<literal>/var/lib/dpkg/info/</literal>.)
</para>
</listitem>
<listitem>
<para>
<literal>grep fu Contents</literal>, o <literal>zgrep fu Contents.gz</literal>.
</para>
<para>
Esto realiza una búsqueda de ficheros que contengan la subcadena
<literal>fu</literal> en cualquier parte de sus caminos completos.  Los
ficheros <literal>Contents</literal> y <literal>Contents.gz</literal> residen
en los directorios principales (stable, non-free, contrib, development) de un
sitio FTP de Debian.  Cada fichero <literal>Contents</literal> se refiere
únicamente a los paquetes que se encuentran bajo el árbol de directorios en
el que se encuentra.  Por ello, es posible que tenga que buscar más de un
fichero <literal>Contents</literal> para encontrar cual es el paquete que tiene
el fichero <literal>fu</literal>.
</para>
<para>
Este método tiene como ventaja sobre <literal>dpkg --search</literal> que es
capaz de encontrar ficheros contenidos en paquetes que no han sido instalados
previamente en el sistema.
</para>
</listitem>
</itemizedlist>
</section>

</chapter>

