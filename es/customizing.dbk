<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="customizing"><title>Personalización de la instalación de Debian GNU/Linux</title>
<section id="papersize"><title>¿Cómo puedo asegurarme de que todos los programas usen el mismo tamaño de papel?</title>
<para>
Instale el paquete <systemitem role="package">libpaper1</systemitem>, y le
pedirá un tamaño de papel predeterminado para todo el sistema.  Este valor se
guardará en el fichero <literal>/etc/papersize</literal>.
</para>
<para>
Puede especificarse también mediante la variable de entorno
<literal>PAPERSIZE</literal>.  Para más detalles consulte la página de manual
<citerefentry><refentrytitle>papersize</refentrytitle><manvolnum>5</manvolnum></citerefentry>.
</para>
</section>

<section id="hardwareaccess"><title>¿Cómo puedo proporcionar acceso a periféricos de hardware sin comprometer la seguridad?</title>
<para>
Muchos ficheros de dispositivos del directorio <literal>/dev</literal>
pertenecen a ciertos grupos predefinidos.  Por ejemplo
<literal>/dev/fd0</literal> pertenece al grupo <literal>floppy</literal>, y
<literal>/dev/dsp</literal> pertenece al grupo <literal>audio</literal>.
</para>
<para>
Para que un usuario determinado tenga acceso a uno de dichos dispositivos,
añada al usuario al grupo al que pertenece el dispositivo, es decir:
</para>
<screen>
adduser usuario grupo
</screen>
<para>
Esto evita tener que ejecutar chmod sobre el dispositivo.
</para>
</section>

<section id="consolefont"><title>¿Cómo cargo una fuente de consola en el arranque al estilo Debian?</title>
<para>
Los paquetes <systemitem role="package">kbd</systemitem> y <systemitem
role="package">console-tools</systemitem> ahora admiten esto, edite el fichero
<literal>/etc/kbd/config</literal> o
<literal>/etc/console-tools/config</literal>.
</para>
</section>

<section id="appdefaults"><title>¿Cómo puedo configurar los valores por omisión de una aplicación X11?</title>
<para>
Los programas X de Debian instalan sus ficheros de recursos en el directorio
<literal>/etc/X11/app-defaults/</literal>.  Si quiere personalizar aplicaciones
X de forma global, cambie dichos ficheros.  Están marcados como ficheros de
configuración, así que su contenido se mantendrá entre actualizaciones.
</para>
</section>

<section id="booting"><title>Cada distribución parece tener un método diferente para `arrancar'. Cuénteme acerca del usado por Debian.</title>
<para>
Como todo UNIX, Debian arranca ejecutando el programa <literal>init</literal>.
El fichero de configuración para <literal>init</literal> (que es
<literal>/etc/inittab</literal>) especifica que el primer script que debe
ejecutarse será <literal>/etc/init.d/rcS</literal>.  Este script verifica y
monta los sistemas de ficheros, carga módulos del núcleo, comienza los
servicios de red (llamando al script <literal>/etc/init.d/network</literal>),
programa el reloj, inicializa alguna otra cosa, y luego ejecuta todos los
scripts (excepto aquellos con un '.'  en el nombre) en
<literal>/etc/rc.boot/</literal>.  Estos scripts especifican el teclado a
usarse, recuperan ficheros perdidos estando en un editor, y configuran los
puertos serie.  Después de completar el arranque, <literal>init</literal>
ejecuta todos los scripts de inicio de un directorio indicado por el runlevel
predeterminado (este valor se especifica por la entrada <literal>id</literal>
en <literal>/etc/inittab</literal>).  Como la mayoría de los UNIX compatibles
con System V, Linux tiene 7 runlevels: 0 (detiene el sistema), 1 (modo
único-usuario), de 2 a 5 (varios modos multi-usuario), y 6 (reinicializar el
sistema).  Los sistemas Debian vienen configurados con id=2, lo que indica que
el runlevel será de `2' al entrar al estado multi-usuario, y que se
ejecutarán los scripts en <literal>/etc/rc2.d/</literal>.
</para>
<para>
De hecho, los scripts en cualquiera de los directorios
<literal>/etc/rcN.d/</literal> son sólo enlaces simbólicos de vuelta a los
scripts en <literal>/etc/init.d/</literal>.  Sin embargo, los
<emphasis>nombres</emphasis> de los ficheros en cada uno de los directorios
<literal>/etc/rcN.d/</literal> están elegidos para indicar la
<emphasis>manera</emphasis> en que los scripts en
<literal>/etc/init.d/</literal> serán ejecutados.  Específicamente, antes de
entrar a cualquier runlevel, se ejecutan todos los scripts cuyo nombre comienza
con 'K'; estos scripts detienen servicios.  Luego se ejecutan todos los scripts
cuyo nombre comienza con 'S'; estos scripts inician servicios.  El número de
dos dígitos que sigue a la 'K' o 'S' indica el orden en que los scripts se
ejecutarán.  Los de números más bajos se ejecutarán primero.
</para>
<para>
Esta estrategia funciona porque los scripts en <literal>/etc/init.d/</literal>
llevan todos un argumento que puede ser `start' (comenzar), `stop' (terminar),
o `reload' (reiniciar), y llevarán a cabo la tarea indicada por éste.  Por
ejemplo, con el argumento `reload' la orden <literal>/etc/init.d/sendmail
reload</literal> envía al daemon sendmail un señal que le hace releer su
fichero de configuración.  Estos scripts se pueden usar para controlar varios
procesos incluso después de que el sistema haya sido iniciado.
</para>
</section>

<section id="custombootscripts"><title>Parece ser que Debian no usa <literal>rc.local</literal> para personalizar el proceso de inicialización, ¿qué facilidades provee Debian para esta tarea?</title>
<para>
Suponga que un sistema necesita ejecutar el script <literal>fu</literal> al
inicializar, o al entrar en un runlevel en particular.  Entonces el
administrador del sistema debería:
</para>
<itemizedlist>
<listitem>
<para>
Colocar el script <literal>fu</literal> en el directorio
<literal>/etc/init.d/</literal>.
</para>
</listitem>
<listitem>
<para>
Ejecutar la orden <literal>update-rc.d</literal> con argumentos apropiados para
preparar enlaces entre los directorios rc?.d (especificados desde la línea de
órdenes) y <literal>/etc/init.d/fu</literal>.  Aquí, '?'  es un número de 0
a 6 y coresponde a un runlevel estilo System V.
</para>
</listitem>
<listitem>
<para>
Reinicializar el sistema.
</para>
</listitem>
</itemizedlist>
<para>
La orden <literal>update-rc.d</literal> creará enlaces entre ficheros en los
directorios rc?.d y el script en <literal>/etc/init.d/</literal>.  Cada enlace
comenzará con una 'S' o una 'K', seguida de un número, seguido por el nombre
del script.  Los scripts que comiencen con 'S' en
<literal>/etc/rcN.d/</literal> serán ejecutados al entrar al runlevel
<literal>N</literal>.  Los que lo hagan con con una 'K' serán ejecutados al
dejar el runlevel <literal>N</literal>.
</para>
<para>
Uno podría, por ejemplo, obligar al script <literal>fu</literal> a ejecutarse
en el arranque, poniéndolo en <literal>/etc/init.d/</literal> e instalando los
enlaces con <literal>update-rc.d fu defaults 19</literal>.  El argumento
'defaults' se refiere a los runlevels predeterminados, que son los que van del
2 al 5.  El argumento '19' se asegura de que <literal>fu</literal> sea llamado
antes que cualquier otro script que contenga el número 20 o un número mayor.
</para>
</section>

<section id="divert"><title>¿Cómo puedo reemplazar un fichero instalado por un paquete con otro?</title>
<para>
Suponga que el administrador o un usuario local desea usar el programa
"login-local" en lugar del "login" provisto por el paquete Debian
<literal>login</literal>.  No:
</para>
<itemizedlist>
<listitem>
<para>
Sobreescriba <literal>/bin/login</literal> con <literal>login-local</literal>.
</para>
</listitem>
</itemizedlist>
<para>
El sistema administrador de paquetes no tendrá conocimiento acerca de este
cambio, y simplemente reescribira su <literal>/bin/login</literal> cuando
<literal>login</literal> (o cualquier paquete que provea
<literal>/bin/login</literal>) sea instalado o actualizado.  En lugar de eso,
</para>
<itemizedlist>
<listitem>
<para>
Ejecute <literal>dpkg-divert --divert /bin/login.debian /bin/login</literal>
para indicar que todas las futuras instalaciones de paquetes
<literal>login</literal> escriban el fichero <literal>/bin/login</literal> en
<literal>/bin/login.debian</literal>.
</para>
</listitem>
<listitem>
<para>
Luego ejecute <literal>cp login-local /bin/login</literal> para mover su
versión localmente compilada a su lugar.
</para>
</listitem>
</itemizedlist>
<para>
El mensaje acerca del uso de <literal>dpkg-divert</literal> provee más
detalles (los cuales se pueden ver usando la orden <literal>dpkg-divert
--help</literal>).  Hay más información disponible en el <ulink
url="ftp://ftp.debian.org/debian/doc/package-developer/programmer.ps.gz">ftp://ftp.debian.org/debian/doc/package-developer/programmer.ps.gz</ulink>.
</para>
</section>

<section id="s10.8"><title>¿Cómo puedo incluir el paquete Debian que creé localmente en la lista de paquetes disponibles usada por el sistema de administración de paquetes?</title>
<para>
Puede hacer esto de cualquiera de las siguientes dos maneras:
</para>
<itemizedlist>
<listitem>
<para>
Use <literal>dselect</literal>, y vuelva a seleccionar el método de acceso.
Se le preguntará por un directorio en donde sus paquetes locales residen.
</para>
</listitem>
<listitem>
<para>
Ejecute la orden <literal>dpkg-scanpackages DIR_BIN FICHERO_DE_REEMPLAZOS
[PREFIJO] > Packages.nuevo</literal> donde:
</para>
<itemizedlist>
<listitem>
<para>
DIR-BIN es un directorio donde se hayan almacenados archivos Debian (que
normalmente tendrán la extensión ".deb").
</para>
</listitem>
<listitem>
<para>
FICHERO_DE_REEMPLAZOS es un fichero que es editado por los que mantienen la
distribución y normalmente se almacena en un archivo ftp Debian en
<literal>indices/override.main.gz</literal> para la distribución principal
("main").
</para>
</listitem>
<listitem>
<para>
PREFIJO es una secuencia de caracteres opcional que puede añadirse en el
fichero Packages.new que está siendo producido.
</para>
</listitem>
</itemizedlist>
<para>
Una vez que haya armado el fichero <literal>Packages.nuevo</literal>, avise al
sistema administrador de paquetes sobre él con la orden <literal>dpkg
--update-avail Packages.nuevo</literal>.
</para>
</listitem>
</itemizedlist>
</section>

<section id="diverse"><title>A algunos usuarios les gusta mawk, a otros les gusta gawk; algunos prefieren vim mientras que otros prefieren elvis; algunos quieren usar trn, a otros les gusta tin; ¿cómo soporta Debian la diversidad?</title>
<para>
Hay muchos casos en donde dos paquetes proveen dos versiones diferentes de un
programa, los cuales proveen la misma funcionalidad básica.  Los usuarios
podrían preferir uno sobre el otro por hábito, o porque la interfaz de uno es
algo más agradable que la de otro.  Otros usuarios en el mismo sistema pueden
elegir diferente.
</para>
<para>
Debian usa un sistema de paquetes "virtual" para permitir a los administradores
elegir (o dejar que los usuarios elijan) sus herramientas favoritas cuando hay
dos o más que proveen la misma funcionalidad básica, y al mismo tiempo
permitir que se satisfagan las dependencias sin necesidad de especificar un
paquete en particular.
</para>
<para>
Por ejemplo, podrían existir dos versiones diferentes de lectores de news en
un sistema.  El paquete servidor de news podría `recomendar' que exista
<emphasis>algún</emphasis> lector en el sistema, pero la elección de
<literal>tin</literal> o <literal>trn</literal> se puede dejar a cada usuario.
Esto se logra haciendo que ambos paquetes provean el paquete virtual
<literal>news-reader</literal>.  <emphasis>Cuál</emphasis> de los programas es
invocado será determinado por un enlace apuntando desde un fichero con el
nombre del paquete virtual <literal>/etc/alternatives/news-reader</literal> al
fichero seleccionado, p.  ej.  <literal>/usr/bin/trn</literal>.
</para>
<para>
Un simple enlace es insuficiente para soportar el uso completo de un programa
alternativo, normalmente páginas de manual y posiblemente otros ficheros de
soporte pueden ser seleccionados también.  El script Perl
<literal>update-alternatives</literal> provee una manera de asegurar que todos
los ficheros asociados con un paquete específico sean seleccionados como valor
por omisión en el sistema.
</para>
</section>

</chapter>

