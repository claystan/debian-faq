<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="faqinfo"><title>Información general sobre la FAQ</title>
<section id="authors"><title>Autores</title>
<para>
La primera edición de esta FAQ fue escrita y mantenida por J.H.M.  Dassen
(Ray) y Chuck Stickelman.  Los autores de la FAQ reescrita son Susan G.
Kleinmann and Sven Rudolph.  Después de ellos, la FAQ estuvo mantenida por
Santiago Vila.  El encargado actual es Josip Rodin.
</para>
<para>
Partes de la información provienen de
</para>
<itemizedlist>
<listitem>
<para>
El anuncio de publicación de Debian-1.1, de <ulink
url="http://www.perens.com/">Bruce Perens</ulink>.
</para>
</listitem>
<listitem>
<para>
La FAQ de Linux, de <ulink
url="http://www.chiark.greenend.org.uk/~ijackson/">Ian Jackson</ulink>.
</para>
</listitem>
<listitem>
<para>
Los <ulink url="http://lists.debian.org/">archivos de las listas de correo de
Debian</ulink>.
</para>
</listitem>
<listitem>
<para>
El manual del programador de dpkg y el manual de normas de Debian (véase <xref
linkend="debiandocs"/>).
</para>
</listitem>
<listitem>
<para>
Los muchos desarrolladores, voluntarios y probadores beta.
</para>
</listitem>
<listitem>
<para>
Los descascarados recuerdos de sus autores.
</para>
</listitem>
</itemizedlist>
<para>
Los autores quieren agradecer a todos aquellos que hicieron posible este
documento.
</para>
</section>

<section id="feedback"><title>¿A dónde debo enviar preguntas, correcciones, etc. sobre esta traducción?</title>
<para>
Esta traducción ha sido realizada por el grupo de hispanización de Debian.
Para ponerse en contacto con sus miembros, por favor utilice la lista de correo
electrónico <ulink
url="mailto:debian-l10n-spanish@lists.debian.org">debian-l10n-spanish@lists.debian.org</ulink>.
Si desea colaborar con nosotros, puede subscribirse a dicha lista de correo
electrónico enviando un mensaje a <ulink
url="mailto:debian-l10n-spanish-REQUEST@lists.debian.org">debian-l10n-spanish-REQUEST@lists.debian.org</ulink>
con la palabra <emphasis>subscribe</emphasis> en el cuerpo del mensaje.  Esta
traducción ha sido llevada a cabo por:
</para>
<itemizedlist>
<listitem>
<para>
César Ballardini (<ulink
url="mailto:cballard@santafe.com.ar">cballard@santafe.com.ar</ulink>),
</para>
</listitem>
<listitem>
<para>
Ricard Lluis Fuertes Mulet (<ulink
url="mailto:richy@millorsoft.es">richy@millorsoft.es</ulink>),
</para>
</listitem>
<listitem>
<para>
Luis Francisco González (<ulink
url="mailto:luisgh@cogs.susx.ac.uk">luisgh@cogs.susx.ac.uk</ulink>),
</para>
</listitem>
<listitem>
<para>
Nicolás Lichtmaier (<ulink
url="mailto:nick@Feedback.com.ar">nick@Feedback.com.ar</ulink>),
</para>
</listitem>
<listitem>
<para>
Juan Ignacio Llona (<ulink url="mailto:jillona@jet.es">jillona@jet.es</ulink>),
</para>
</listitem>
<listitem>
<para>
Carlos Martínez Txakartegi (<ulink
url="mailto:txakar@redestb.es">txakar@redestb.es</ulink>) y
</para>
</listitem>
<listitem>
<para>
Enrique Zanardi (<ulink
url="mailto:ezanard@debian.org">ezanard@debian.org</ulink>).
</para>
</listitem>
</itemizedlist>
</section>

<section id="latest"><title>Disponibilidad</title>
<para>
La última versión en inglés de este documento está disponible en <ulink
url="http://www.debian.org/doc/FAQ/">http://www.debian.org/doc/FAQ/</ulink>.
</para>
<para>
También está disponible para descarga en formato HTML, PostScript, .dvi y
texto `plano' puede encontrarlo en el <ulink
url="ftp://ftp.debian.org/debian/">servidor FTP de Debian</ulink> y en
cualquiera de sus <ulink
url="http://www.debian.org/distrib/ftplist.html">réplicas</ulink> en el
directorio <literal><ulink
url="ftp://ftp.debian.org/debian/doc/FAQ/">doc/FAQ/</ulink></literal>.  Los
ficheros originales SGML que se utilizan para crear este documento se
encuentran disponibles en el código fuente del paquete
<literal>doc-debian</literal>.  El paquete <literal>sgml-tools</literal>
contiene herramientas que permiten transformar este documento a otros formatos,
a petición del usuario: ficheros info de GNU, HTML, LaTeX, formato .dvi de
TeX, y PostScript.
</para>
</section>

<section id="docformat"><title>¿En qué formato está escrito este documento?</title>
<para>
Este documento se escribió utilizando el DTD de Linuxdoc-SGML.  El sistema
Linuxdoc-SGML (ahora llamado SGML-Tools) puede utilizarse para crear ficheros
en una variedad de formatos distintos, por ejemplo info de GNU, HTML, LaTeX, el
formato .dvi de TeX, y ficheros PostScript (TM).  SGML-Tools está disponible
como paquete Debian.
</para>
<section id="s15.4.1"><title>¿A dónde debo enviar preguntas, correcciones, etc. sobre [la versión inglesa de] este documento?</title>
<para>
Los comentarios sobre este documento son bienvenidos.  Envíelos por favor
mediante correo electrónico a <ulink
url="mailto:doc-debian@packages.debian.org">doc-debian@packages.debian.org</ulink>,
o mejor, envíe un bicho de tipo wishlist acerca del paquete
<literal>doc-debian</literal>.
</para>
</section>

<section id="redis"><title>¿(Cómo) Puedo redistribuir este fichero?</title>
<para>
(Nota del traductor: La traducción de los párrafos de esta subsección no
está aprobada por la Free Software Foundation, y por lo tanto carece de valor
legal a todos sus efectos.  Se ha incluído a título didáctico para beneficio
de los lectores hispanoparlantes que no pueden leer el original en inglés.
Para cualquier duda sobre el estado técnico legal en cuanto a Propiedad
Intelectual la única fuente de consulta es el original en inglés, que se
transcribe a continuación de la traducción.) Este documento tiene copyright
1996, 1997, 1998, 1999 por parte de SPI (vea la sección <xref linkend="SPI"/>,
software de interés público).  Se otorga el permiso de distribuir copias
literales de este documento siempre que se preserve en todas las copias el
aviso de copyright y este aviso de permiso.  Se otorga el permiso de copiar y
distribuir versiones modificadas de este documento bajo las condiciones de las
copias literales, si se asegura que el trabajo derivado resultante completo se
distribuye bajo los términos de una nota de permiso idéntica a esta misma.
Se otorga el permiso de copiar y distribuir traducciones de este documento en
otro idioma bajo las condiciones de las versiones modificadas, excepto que esta
nota de permiso puede ser una de las traducciones aprobadas por la Free
Software Foundation en lugar de sus originales en inglés.
</para>
<para>
This document is copyright 1996, 1997, 1998 by SPI (see section <xref
linkend="SPI"/>).  Permission is granted to make and distribute verbatim copies
of this document provided the copyright notice and this permission notice are
preserved on all copies.  Permission is granted to copy and distribute modified
versions of this document under the conditions for verbatim copying, provided
that the entire resulting derived work is distributed under the terms of a
permission notice identical to this one.  Permission is granted to copy and
distribute translations of this document into another language, under the above
conditions for modified versions, except that this permission notice may be
included in translations approved by the Free Software Foundation instead of in
the original English.
</para>
</section>

</section>

<section id="latesttranslated"><title>¿Dónde puedo conseguir la última versión de esta traducción?</title>
<para>
Esta traducción se distribuye en el paquete Debian denominado
<literal>doc-debian-es</literal>.
</para>
</section>

</chapter>

