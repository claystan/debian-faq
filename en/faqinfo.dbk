<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>

<chapter id="faqinfo"><title>General information about the FAQ</title>
<section id="authors"><title>Authors</title>
<para>
The first edition of this FAQ was made and maintained by J.H.M. Dassen (Ray)
and Chuck Stickelman.  Authors of the rewritten &debian; FAQ are Susan
G. Kleinmann and Sven Rudolph.  After them, the FAQ was maintained by Santiago
Vila and, later, by Josip Rodin.  The current maintainer is Javier
Fernandez-Sanguino.
</para>
<para>
Parts of the information came from:
</para>
<itemizedlist>
<listitem>
<para>
The Debian-1.1 release announcement, by <ulink
url="https://perens.com/">Bruce Perens</ulink>,
</para>
</listitem>
<listitem>
<para>
the Linux FAQ, by <ulink url="https://www.chiark.greenend.org.uk/~ijackson/">Ian
Jackson</ulink>,
</para>
</listitem>
<listitem>
<para>
<ulink url="&url-lists-debian;">Debian Mailing Lists Archives</ulink>,
</para>
</listitem>
<listitem>
<para>
the dpkg programmers' manual and the Debian Policy manual (see <xref
linkend="debiandocs"/>),
</para>
</listitem>
<listitem>
<para>
many developers, volunteers, and beta testers,
</para>
</listitem>
<listitem>
<para>
the flaky memories of its authors.  :-)
</para>
</listitem>
<listitem>
<para>
and the <ulink
url="http://KamarajuKusumanchi.github.io/choosing_debian_distribution/choosing_debian_distribution.html">Choosing
a Debian distribution FAQ</ulink>, which Kamaraju Kusumanchi graciously
released under the GPL, so it could be included as a new chapter (see <xref
linkend="choosing"/>).
</para>
</listitem>
</itemizedlist>
<para>
The authors would like to thank all those who helped make this document
possible.
</para>
<para>
All warranties are disclaimed.  All trademarks are property of their respective
trademark owners.
</para>
</section>

<section id="feedback"><title>Feedback</title>
<para>
Comments and additions to this document are always welcome.  Please send e-mail
to <email>doc-debian@packages.debian.org</email>, or submit a wishlist bug
report against the <systemitem role="package"><ulink
url="https://bugs.debian.org/debian-faq">debian-faq</ulink></systemitem>
package.
</para>
</section>

<section id="latest"><title>Availability</title>
<para>
The latest version of this document can be viewed on the Debian WWW pages at
<ulink url="&url-debian-faq;"/>.
</para>
<para>
It is also available for download in plain text, HTML, and PDF
formats at <ulink url="&url-debian-manuals-faq;"/>.
Also, there are several translations there.
</para>
<para>
This document is available in the <systemitem
role="package">debian-faq</systemitem> package.  Translations are available in
<systemitem role="package">debian-faq-de</systemitem>, <systemitem
role="package">debian-faq-fr</systemitem> and other packages.
</para>
<para>
The original XML files used to create this document are also available in
<systemitem role="package">debian-faq</systemitem>'s source package, or in GIT
at: <literal>git@salsa.debian.org:ddp-team/debian-faq.git</literal> and
<ulink url="https://salsa.debian.org/ddp-team/debian-faq"/>.
</para>
</section>

<section id="docformat"><title>Document format</title>
<para>
This document was written using the DocBook XML DTD.  This system enables us
to create files in a variety of formats from one source, e.g. this document
can be viewed as HTML, plain text, TeX DVI, PostScript, PDF, or GNU info.
</para>
</section>

</chapter>
