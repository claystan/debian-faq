<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>

<chapter id="choosing"><title>Choosing a Debian distribution</title>
<para>
There are many different Debian distributions.  Choosing the proper Debian
distribution is an important decision.  This section covers some information
useful for users that want to make the choice best suited for their system and
also answers possible questions that might be arising during the process.  It
does not deal with "why you should choose Debian" but rather "which
distribution of Debian".
</para>
<para>
For more information on the available distributions read <xref
linkend="dists"/>.
</para>
<section id="s3.1"><title>Which Debian distribution (stable/testing/unstable) is better for me?</title>
<para>
The answer is a bit complicated.  It really depends on what you intend to do.
One solution would be to ask a friend who runs Debian.  But that does not mean
that you cannot make an independent decision.  In fact, you should be able to
decide once you complete reading this chapter.
</para>
<itemizedlist>
<listitem>
<para>
If security or stability are at all important for you: install stable.  period.
This is the most preferred way.
</para>
</listitem>
<listitem>
<para>
If you are a new user installing to a desktop machine, start with stable.  Some
of the software is quite old, but it's the least buggy environment to work in.
You can easily switch to the more modern unstable (or testing) once you are a
little more confident.
</para>
</listitem>
<listitem>
<para>
If you are a desktop user with a lot of experience in the operating system and
do not mind facing the odd bug now and then, or even full system breakage, use
unstable.  It has all the latest and greatest software, and bugs are usually
fixed swiftly.
</para>
</listitem>
<listitem>
<para>
If you are running a server, especially one that has strong stability
requirements or is exposed to the Internet, install stable.  This is by far the
strongest and safest choice.
</para>
</listitem>
</itemizedlist>
<para>
The following questions (hopefully) provide more detail on these choices.
After reading this whole FAQ, if you still could not make a decision, stick
with the stable distribution.
</para>
<section id="s3.1.1"><title>You asked me to install stable, but in stable so and so hardware is not detected/working. What should I do?</title>
<para>
Try to search the web using a search engine and see if someone else is able to
get it working in stable.  Most of the hardware should work fine with stable.
But if you have some state-of-the-art, cutting edge hardware, it might not work
with stable.  If this is the case, you might want to install/upgrade to either
testing or unstable.
</para>
<para>
For laptops, <ulink url="https://www.linux-on-laptops.com/"/>
is a very good website to see if someone else is able to get it to work under
Linux.  The website is not specific to Debian, but is nevertheless a tremendous
resource.  I am not aware of any such website for desktops.
</para>
<para>
Another option would be to ask in the debian-user mailing list by sending an
email to debian-user@lists.debian.org.  Messages can be posted to the list even
without subscribing.  The archives can be read through <ulink url="https://lists.debian.org/debian-user/"/>.
Information regarding subscribing to the list can be found at the location of
archives.  You are strongly encouraged to post your questions on the
mailing-list rather than on <ulink
url="&url-debian-support;">irc</ulink>.  The mailing-list messages are
archived, so the solution to your problem can help others with the same issue.
</para>
</section>

<section id="s3.1.2"><title>Will there be different versions of packages in different distributions?</title>
<para>
Yes.  Unstable has the most recent (latest) versions.  But the packages in
unstable are not well tested and might have bugs.
</para>
<para>
On the other hand, stable contains old versions of packages.  But this package
is well tested and is less likely to have any bugs.
</para>
<para>
The packages in testing fall between these two extremes.
</para>
</section>

<section id="s3.1.3"><title>The stable distributions really contains outdated packages. Just look at Kde, Gnome, Xorg or even the kernel. They are very old. Why is it so?</title>
<para>
Well, you might be correct.  The age of the packages at stable depends on when
the last release was made.  Since there is typically over 1 year between
releases you might find that stable contains old versions of packages.
However, they have been tested in and out.  One can confidently say that the
packages do not have any known severe bugs, security holes etc., in them.  The
packages in stable integrate seamlessly with other stable packages.  These
characteristics are very important for production servers which have to work 24
hours a day, 7 days a week.
</para>
<para>
On the other hand, packages in testing or unstable can have hidden bugs,
security holes etc.  Moreover, some packages in testing and unstable might not
be working as intended.  Usually people working on a single desktop prefer
having the latest and most modern set of packages.  Unstable is the solution
for this group of people.
</para>
<para>
As you can see, stability and novelty are two opposing ends of the spectrum.
If stability is required: install stable distribution.  If you want to work
with the latest packages, then install unstable.
</para>
</section>

<section id="s3.1.4"><title>If I were to decide to change to another distribution, can I do that?</title>
<para>
Yes, but it is a one way process.  You can go from stable --&gt; testing --&gt;
unstable.  But the reverse direction is not "possible".  So better be sure if
you are planning to install/upgrade to unstable.
</para>
<para>
Actually, if you are an expert and if you are willing to spend some time and if
you are real careful and if you know what you are doing, then it might be
possible to go from unstable to testing and then to stable.  The installer
scripts are not designed to do that.  So in the process, your configuration
files might be lost and...
</para>
</section>

<section id="s3.1.5"><title>Could you tell me whether to install stable, testing or unstable?</title>
<para>
No.  This is a rather subjective issue.  There is no perfect answer as it
depends on your software needs, your willingness to deal with possible
breakage, and your experience in system administration.  Here are some tips:
</para>
<itemizedlist>
<listitem>
<para>
Stable is rock solid.  It does not break and has full security support.  But it
not might have support for the latest hardware.
</para>
</listitem>
<listitem>
<para>
Testing has more up-to-date software than Stable, and it breaks less often than
Unstable.  But when it breaks, it might take a long time for things to get
rectified.  Sometimes this could be days and it could be months at times.  It
also does not have permanent security support.
</para>
</listitem>
<listitem>
<para>
Unstable has the latest software and changes a lot.  Consequently, it can break
at any point.  However, fixes get rectified in many occasions in a couple of
days and it always has the latest releases of software packaged for Debian.
</para>
</listitem>
</itemizedlist>
<para>
When deciding between testing and unstable bear in mind that there might be
times when tracking testing would be beneficial as opposed to unstable.  One of
this document's authors experienced such situation due to the gcc transition
from gcc3 to gcc4.  He was trying to install the <systemitem
role="package">labplot</systemitem> package on a machine tracking unstable and
it could not be installed in unstable as some of its dependencies have
undergone gcc4 transition and some have not.  But the package in testing was
installable on a testing machine as the gcc4 transitioned packages had not
"trickled down" to testing.
</para>
</section>

<section id="s3.1.6"><title>You are talking about testing being broken. What do you mean by that?</title>
<para>
Sometimes, a package might not be installable through package management tools.
Sometimes, a package might not be available at all, maybe it was (temporarily)
removed due to bugs or unmet dependencies.  Sometimes, a package installs but
does not behave in the proper way.
</para>
<para>
When these things happen, the distribution is said to be broken (at least for
this package).
</para>
</section>

<section id="s3.1.7"><title>Why is it that testing could be broken for months? Won't the fixes introduced in unstable flow directly down into testing?</title>
<para>
The bug fixes and improvements introduced in the unstable distribution trickle
down to testing after a certain number of days.  Let's say this threshold is 5
days.  The packages in unstable go into testing only when there are no RC-bugs
reported against them.  If there is a RC-bug filed against a package in
unstable, it will not go into testing after the 5 days.
</para>
<para>
The idea is that, if the package has any problems, it would be discovered by
people using unstable and will be fixed before it enters testing.  This keeps
testing in a usable state for most of the time.  Overall a brilliant concept,
if you ask me.  But things aren't always that simple.  Consider the following
situation:
</para>
<itemizedlist>
<listitem>
<para>
Imagine you are interested in package XYZ.
</para>
</listitem>
<listitem>
<para>
Let's assume that on June 10, the version in testing is XYZ-3.6 and in unstable
it is XYZ-3.7.
</para>
</listitem>
<listitem>
<para>
After 5 days, XYZ-3.7 from unstable migrates into testing.
</para>
</listitem>
<listitem>
<para>
So on June 15, both testing and unstable have XYZ-3.7 in their repositories.
</para>
</listitem>
<listitem>
<para>
Let's say, the user of testing distribution sees that a new XYZ package is
available and updates his XYZ-3.6 to XYZ-3.7.
</para>
</listitem>
<listitem>
<para>
Now on June 25, someone using testing or unstable discovers an RC bug in
XYZ-3.7 and files it in the BTS.
</para>
</listitem>
<listitem>
<para>
The maintainer of XYZ fixes this bug and uploads it to unstable say on June 30.
Here it is assumed that it takes 5 days for the maintainer to fix the bug and
upload the new version.  The number 5 should not be taken literally.  It could
be less or more, depending upon the severity of the RC-bug at hand.
</para>
</listitem>
<listitem>
<para>
This new version in unstable, XYZ-3.8 is scheduled to enter testing on July
5th.
</para>
</listitem>
<listitem>
<para>
But on July 3rd some other person discovers another RC-bug in XYZ-3.8.
</para>
</listitem>
<listitem>
<para>
Let's say the maintainer of XYZ fixes this new RC-bug and uploads new version
of XYZ after 5 days.
</para>
</listitem>
<listitem>
<para>
So on July 8th, testing has XYZ-3.7 while unstable has XYZ-3.9.
</para>
</listitem>
<listitem>
<para>
This new version XYZ-3.9 is now rescheduled to enter testing on July 13th.
</para>
</listitem>
<listitem>
<para>
Now since you are running testing, and since XYZ-3.7 is buggy, you could
probably use XYZ only after July 13th.  That is you essentially ended up with a
broken XYZ for about one month.
</para>
</listitem>
</itemizedlist>
<para>
The situation can get much more complicated, if say, XYZ depends on 4 other
packages.  This could in turn lead to an unusable testing distribution for
months.  While the scenario above is immaginary, similar things can occur in
real life, though they are rare.
</para>
</section>

<section id="s3.1.8"><title>From an administrator's point of view, which distribution requires more attention?</title>
<para>
One of the main reasons why many people choose Debian over other Linux
distributions is that it requires very little administration.  People want a
system that just works.  In general one can say that stable requires very
little maintenance, while testing and unstable require constant maintenance
from the administrator.  If you are running stable, all you need to worry about
is keeping track of security updates.  If you are running either testing or
unstable it is a good idea to be aware of the new bugs discovered in the
installed packages, new bugfixes/features introduced etc.
</para>
</section>

<section id="s3.1.9"><title>What happens when a new release is made?</title>
<para>
This question will not help you in choosing a Debian distribution.  But sooner
or later you will face this question.
</para>
<para>
The stable distribution is currently &releasename;; The next stable distribution will
be called &nextreleasename;.  Let's consider the particular case of what happens when
&nextreleasename; is released as the new stable version.
</para>
<itemizedlist>
<listitem>
<para>
oldstable = &oldreleasename;; stable = &releasename;; testing = &nextreleasename;; unstable = sid
</para>
</listitem>
<listitem>
<para>
Unstable is always referred to as sid irrespective of whether a release is made
or not.
</para>
</listitem>
<listitem>
<para>
Packages constantly migrate from sid to testing (i.e. &nextreleasename;).  But packages
in stable (i.e. &releasename;) remain the same except for security updates.
</para>
</listitem>
<listitem>
<para>
After some time testing becomes frozen.  But it will still be called testing.
At this point no new packages from unstable can migrate to testing unless they
include release-critical (RC) bug fixes.
</para>
</listitem>
<listitem>
<para>
When testing is frozen, all the new bugfixes introduced have to be manually
checked by the members of the release team.  This is done to ensure that there
won't be any unknown severe problems in the frozen testing.
</para>
</listitem>
<listitem>
<para>
RC bugs in 'frozen testing' are reduced to either zero or, if greater than
zero, the bugs are either marked as ignored for the release or are deferred for
a point release.
</para>
</listitem>
<listitem>
<para>
The 'frozen testing' with no rc-bugs will be released as the new stable
version.  In our example, this new stable release will be called &nextreleasename;.
</para>
</listitem>
<listitem>
<para>
At this stage oldstable = &releasename;, stable = &nextreleasename;.  The contents of stable and
'frozen testing' are same at this point.
</para>
</listitem>
<listitem>
<para>
A new testing is based on the old testing.
</para>
</listitem>
<listitem>
<para>
Packages start coming down from sid to testing and the Debian community will be
working towards making the next stable release.
</para>
</listitem>
</itemizedlist>
</section>

<section id="s3.1.10"><title>I have a working Desktop/cluster with Debian installed. How do I know which distribution I am running?</title>
<para>
In most situations it is very easy to figure this out.  Take a look at the
<filename>/etc/apt/sources.list</filename> file.  There will be an entry
similar to this:
</para>
<screen>
deb http://ftp.us.debian.org/debian/ unstable main contrib
</screen>
<para>
The third field ('unstable' in the above example) indicates the Debian
distribution the system is currently tracking.
</para>
<para>
You can also use <command>lsb_release</command> (available in the <systemitem
role="package">lsb-release</systemitem> package).  If you run this program in
an unstable system you will get:
</para>
<screen>
$ lsb_release  -a
LSB Version:    core-2.0-noarch:core-3.0-noarch:core-3.1-noarch:core-2.0-ia32:core-3.0-ia32:core-3.1-ia32
Distributor ID: Debian
Description:    Debian GNU/Linux unstable (sid)
Release:    unstable
Codename:   sid
</screen>
<para>
However, this is not always that easy.  Some systems might have
<filename>sources.list</filename> files with multiple entries corresponding to
different distributions.  This could happen if the administrator is tracking
different packages from different Debian distributions.  This is frequently
referred to as apt-pinning.  These systems might run a mixture of
distributions.
</para>
</section>

<section id="s3.1.11"><title>I am currently tracking stable. Can I change to testing or unstable? If so, how?</title>
<para>
If you are currently running stable, then in the
<filename>/etc/apt/sources.list</filename> file the third field will be either
'&releasename;' or 'stable'.  You need to change this to the distribution you want to
run.  If you want to run testing, then change the third field of
<filename>/etc/apt/sources.list</filename> to 'testing'.  If you want to run
unstable, then change the third field to 'unstable'.
</para>
<para>
Currently testing is called &nextreleasename;.  So, if you change the third field of
<filename>/etc/apt/sources.list</filename> to '&nextreleasename;', then also you will be
running testing.  But even when &nextreleasename; becomes stable, you will still be
tracking &nextreleasename;.
</para>
<para>
Unstable is always called Sid.  So if you change the third field of
<filename>/etc/apt/sources.list</filename> to 'sid', then you will be tracking
unstable.
</para>
<para>
Currently Debian offers security updates for testing but not for unstable, as
fixes in unstable are directly made to the main archive.  So if you are running
unstable make sure that you remove the lines relating to security updates in
<filename>/etc/apt/sources.list</filename>.
</para>
<para>
If there is a release notes document available for the distribution you are
upgrading to (even though it has not yet been released) it would be wise to
review it, as it might provide information on how you should upgrade to it.
</para>
<para>
Nevertheless, once you make the above changes, you can run <filename>aptitude
update</filename> and then install the packages that you want.  Notice that
installing a package from a different distribution might automatically upgrade
half of your system.  If you install individual packages you will end up with a
system running mixed distributions.
</para>
<para>
It might be best in some situations to just fully upgrade to the new
distribution running <command>apt full-upgrade</command>, <command>aptitude
safe-upgrade</command> or <command>aptitude full-upgrade</command>.  Read apt's
and aptitude's manual pages for more information.
</para>
</section>

<section id="s3.1.12"><title>I am currently tracking testing (&nextreleasename;). What will happen when a release is made? Will I still be tracking testing or will my machine be running the new stable distribution?</title>
<para>
It depends on the entries in the <filename>/etc/apt/sources.list</filename>
file.  If you are currently tracking testing, these entries are similar to
either:
</para>
<screen>
deb http://ftp.us.debian.org/debian/ testing main
</screen>
<para>
or
</para>
<screen>
deb http://ftp.us.debian.org/debian/ &nextreleasename; main
</screen>
<para>
If the third field in <filename>/etc/apt/sources.list</filename> is 'testing'
then you will be tracking testing even after a release is made.  So after
&nextreleasename; is released, you will be running a new Debian distribution which will
have a different codename.  Changes might not be apparent at first but will be
evident as soon as new packages from unstable go over to the testing
distribution.
</para>
<para>
But if the third field contains '&nextreleasename;' then you will be tracking stable
(since &nextreleasename; will then be the new stable distribution).
</para>
</section>

<section id="s3.1.13"><title>I am still confused. What did you say I should install?</title>
<para>
If unsure, the best bet would be the stable distribution.
</para>
</section>

</section>

<section id="s3.2"><title>But what about Knoppix, Linux Mint Debian Edition, Ubuntu, and others?</title>
<para>
They are not Debian; they are <emphasis>Debian based</emphasis>.  Though there
are many similarities and commonalities between them, there are also crucial
differences.
</para>
<para>
All these distributions have their own merits and are suited to some specific
set of users.  For more information, read <ulink
url="https://www.debian.org/misc/children-distros">Software distributions based
on Debian</ulink> available at the Debian website.
</para>
<section id="s3.2.1"><title>I know that Knoppix/Linux Mint Debian Edition/Ubuntu/... is Debian-based. So after installing it on the hard disk, can I use 'apt' package tools on it?</title>
<para>
These distributions are Debian based.  But they are not Debian.  You will be
still able to use apt package tools by pointing the
<filename>/etc/apt/sources.list</filename> file to these distributions'
repositories.  But then you are not running Debian, you are running a different
distribution.  They are not the same.
</para>
<para>
In most situations if you stick with one distribution you should use that and
not mix packages from other distributions.  Many common breakages arise due to
people running a distribution and trying to install Debian packages from other
distributions.  The fact that they use the same formatting and name (.deb),
does not make them immediately compatible.
</para>
<para>
For example, Knoppix is a Linux distribution designed to be booted as a live CD
whereas Debian is designed to be installed on the hard-disk.  Knoppix is great
if you want to know whether a particular piece of hardware works, or if you
want to experience how a GNU/Linux system 'feels' etc., Knoppix is good for
demonstration purposes while Debian is designed to run 24/7.  Moreover the
number of packages available, the number of architectures supported by Debian
are far more than that of Knoppix.
</para>
<para>
If you want Debian, it is best to install Debian from the get-go.  Although it
is possible to install Debian through other distributions, such as Knoppix, the
procedure calls for expertise.  If you are reading this FAQ, I would assume
that you are new to both Debian and Knoppix.  In that case, save yourself a lot
of trouble later and install Debian right at the beginning.
</para>
</section>

<section id="s3.2.2"><title>I installed Knoppix/Linux Mint Debian Edition/Ubuntu/... on my hard disk. Now I have a problem. What should I do?</title>
<para>
You are advised not to use the Debian forums (either mailing lists or IRC) for
help as people there may base their suggestions on the assumption that you are
running a Debian system.  These "fixes" might not be suited to what you are
running, and might even make your problem worse.
</para>
<para>
Use the forums of the specific distribution you are using first.  If you do not
get help or the help you get does not fix your problem you might want to try
asking in Debian forums, but keep the advice of the previous paragraph in mind.
</para>
</section>

<section id="s3.2.3"><title>I'm using Knoppix/LMDE/Ubuntu/... and now I want to use Debian. How do I migrate?</title>
<para>
Consider the change from a Debian-based distribution to Debian just like a
change from one operating system to another one.  You should make a backup of
all your data and reinstall the operating system from scratch.  You should not
attempt to "upgrade" to Debian using the package management tools as you might
end up with an unusable system.
</para>
<para>
If all your user data (i.e. your <filename>/home</filename>) is under a
separate partition migrating to Debian is actually quite simple, you just have
to tell the installation system to mount (but not reformat) that partition when
reinstalling.  Making backups of your data, as well as your previous system's
configuration (i.e. <filename>/etc/</filename> and, maybe,
<filename>/var/</filename>) is still encouraged.
</para>
</section>

</section>

</chapter>

