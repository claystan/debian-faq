<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="support"><title>Obtendo suporte para o Debian GNU/Linux.</title>
<section id="debiandocs"><title>Que outra documentação existe em/para um sistema Debian?</title>
<itemizedlist>
<listitem>
<para>
Instruções de instalação para a versão atual: veja <ulink
url="http://www.br.debian.org/releases/stable/i386/install">http://www.br.debian.org/releases/stable/i386/install</ulink>.
</para>
</listitem>
<listitem>
<para>
O Manual de empacotamento é a documentação principal sobre os aspectos
técnicos da criação de pacotes Debian, tanto binários como fontes.
</para>
<para>
Você pode encontrá-lo no pacote <systemitem
role="package">packaging-manual</systemitem>, ou em <ulink
url="ftp://ftp.debian.org/debian/doc/package-developer/packaging.html.tar.gz">ftp://ftp.debian.org/debian/doc/package-developer/packaging.html.tar.gz</ulink>.
</para>
</listitem>
<listitem>
<para>
O Policy Manual (Manual de políticas) documenta as exigências políticas para
a distribuição, por exemplo, a estrutura e conteúdos do arquivo Debian,
vários sobre o planejamento do sistema operacional, assim como requisitos
técnicos que cada pacote tem que satisfazer para ser incluído da
distribuição.
</para>
<para>
Obtenha-o através do pacote <systemitem
role="package">debian-policy</systemitem>, ou em <ulink
url="ftp://ftp.debian.org/debian/doc/package-developer/policy.html.tar.gz">ftp://ftp.debian.org/debian/doc/package-developer/policy.html.tar.gz</ulink>.
</para>
</listitem>
<listitem>
<para>
Documentação sobre pacotes Debian instalados: a maioria dos pacotes tem
arquivos que são desempacotados em <literal>/usr/doc/PACKAGE</literal>.
</para>
</listitem>
<listitem>
<para>
Documentação sobre o projeto Linux: o pacote <systemitem
role="package">doc-linux</systemitem> instala todas as mais recentes versões
dos HOWTOs e mini-HOWTOs do <ulink url="http://www.linuxdoc.org">Linux
Documentation Project (Projeto de documentação do Linux)</ulink>.
</para>
</listitem>
<listitem>
<para>
Páginas de manual estilo Unix: a maioria dos comandos tem páginas de manual
escritas no estilo dos arquivos "man" do Unix.  Elas são referenciadas pela
seção do diretório "man" em que residem: por exemplo, foo(3) refere-se à
página de manual que reside em /usr/man/man3, e pode ser chamada através do
comando: <literal>man 3 foo</literal>, ou apenas <literal>man foo</literal> se
a seção 3 for a única a conter uma página sobre <literal>foo</literal>.
</para>
<para>
Pode-se saber qual diretório de <literal>/usr/share/man/</literal> contém uma
certa página de manual executando <literal>man -w foo</literal>.
</para>
<para>
Novos usuários do Debian devem observar que as páginas de manual de muitos
comandos gerais do sistema não estão disponíveis até que estes pacotes
sejam instalados:
</para>
<itemizedlist>
<listitem>
<para>
<literal>man-db</literal>, que contém o próprio programa
<literal>man</literal>, e outros programas para manipular as páginas de
manual.
</para>
</listitem>
<listitem>
<para>
<literal>manpages</literal>, que contém o sistema de páginas de manual.
(veja <xref linkend="nonenglish"/>).
</para>
</listitem>
</itemizedlist>
</listitem>
<listitem>
<para>
Páginas "info" estilo GNU: documentação para vários comandos, especialmente
ferramentas GNU, não está disponível em páginas de manual, e sim em
arquivos "info" que podem ser lidos pela ferramenta GNU
<literal>info</literal>, ou executando <literal>M-x info</literal> dentro do
GNU Emacs, ou com algum outro visualizador de páginas Info.
</para>
<para>
Sua principal vantagem em relação às páginas de manual originais é que
usam um sistema de hipertexto.  Entretanto, <emphasis>não</emphasis> precisam
da WWW; o <literal>info</literal> pode ser executado a partir de um console em
modo texto.  Ele foi projetado por Richard Stallman e precedeu a WWW.
</para>
</listitem>
</itemizedlist>
<para>
Note que você pode acessar muita documentação em seu sistema usando um
browser WWW, através dos comandos "dwww" ou "dhelp", encontrados em seus
respectivos pacotes.
</para>
</section>

<section id="onlineresources"><title>Existem recursos on-line para discussão sobre o Debian?</title>
<para>
Existem várias listas de discussão relacionadas ao Debian.
</para>
<para>
Num sistema que possua o pacote <systemitem
role="package">doc-debian</systemitem> instalado, existe uma lista completa de
listas de discussão em
<filename>/usr/share/doc/debian/mailing-lists.txt</filename>.
</para>
<para>
Para se inscrever na debian-X (para anúncios sobre o X, usuários, etc.),
envie um mail para debian-X-request@lists.debian.org com a palavra "subscribe"
no campo "Assunto:".  Se você possui um browser WWW que possua suporte a
formulários, você pode se inscrever na lista usando o <ulink
url="http://www.debian.org/MailingLists/subscribe">formulário on-line</ulink>.
Você também pode se retirar da lista utilizando o mesmo formulário.
</para>
<para>
O endereço de e-mail do administrador da lista é
<email>listmaster@lists.debian.org</email>, em caso de você ter algum
problema.
</para>
<para>
Arquivos das listas de discussão do Debian estão disponíveis via WWW em
<ulink url="http://lists.debian.org/">http://lists.debian.org/</ulink>.
</para>
<para>
Usuários podem enviar perguntas a mantenedores de pacotes individuais, desde
que seus endereços de e-mail sejam fornecidos no arquivo de controle (veja a
seção <xref linkend="controlfile"/>) que é incluído em cada pacote.  A
pessoa pode também aprender o nome e e-mail do mantenedor procurando nos
arquivo "Packages", pois este arquivo é somente um conjunto dos arquivos de
controle dos pacotes que se encontram em uma árvore de diretórios particular.
Para extrair um arquivo de controle de um pacote Debian particular, use o
comando:
</para>
<screen>
dpkg --info NomeDoPacote_VVV-RRR.deb
</screen>
<para>
Usuários devem enviar perguntas não específicas ao Debian para um dos grupos
USENET do Linux, qeu são nomeados comp.os.linux.* ou linux.*.  Existem várias
listas de newsgroups Usenet de Linux e outros recursos relacionados na WWW, por
exemplo, nos endereços <ulink
url="http://www.linux.org/docs/usenet.html">Linux Online</ulink> e <ulink
url="http://www.linuxjournal.com/helpdesk.php">Linux Journal</ulink>.
</para>
</section>

<section id="mailinglistconduct"><title>Qual é o código de conduta para as listas de discussão?</title>
<para>
Quando usar as listas de discussão do Debian, por favor, siga estas regras:
</para>
<itemizedlist>
<listitem>
<para>
Não brigue; não é agradável.  As pessoas que desenvolvem o Debian são
Voluntárias, doando seus tempos, energia e dinheiro numa tentativa de
desenvolverem juntas o projeto Debian.
</para>
</listitem>
<listitem>
<para>
Não use linguajar inadequado;
</para>
</listitem>
<listitem>
<para>
Tenha certeza de que está utilizando a lista adequada.
<emphasis>Nunca</emphasis> envie sua inscrição para a própria lista.
<footnote><para> Use o endereço
<emphasis>nome_da_lista</emphasis>-REQUEST@lists.debian.org para isso.  </para>
</footnote>
</para>
</listitem>
<listitem>
<para>
Veja a seção <xref linkend="bugreport"/> para informações sobre o relato de
bugs.
</para>
</listitem>
</itemizedlist>
<section id="advertising"><title>Política sobre anúncios em listas de discussão.</title>
<para>
As listas de discussão do Debian GNU/Linux aceitam publicidade comercial
através de pagamento.  Nós oferecemos isenção se você puder nos mostrar o
comprovante de pagamento da doação de $1000 (U.S.) ou mais para a Free
Software Foundation ordenado pela "Debian".  Uma doação por anúncio, por
favor.  Se você não deseja doar, simplesmente envie seu anúncio para a
lista, e o operador das listas de discussão irá cobrar $1999 (U.S.).  O
operador da lista irá doar esse total, menos as despesas recolhimento desse
total, para a FSF.
</para>
<para>
Através do ato de enviar seu anúncio, você concorda em aceitar a
responsabilidade sobre a taxa, você concorda em indenizar o operador das
listas de discussão contra qualquer reivindicação legal de você ou de
outros com relação a seu anúncio, e você concorda em pagar qualquer despesa
legal e empresarial que incorra no recente recolhimento.  Nossa obrigação
para com você é limitada ao esforço de boa-fé de entrega de sua mensagem.
</para>
<para>
Taxas reduzidas e/ou isenção de taxas estão disponíveis para anúncios
relacionados ao Debian.  Você deve consultar com antecedência o operador da
lista de discussão se deseja obter uma redução da taxa ou isenção.
</para>
</section>

</section>

<section id="searchtools"><title>Existe uma maneira rápida de procurar informações sobre o Debian GNU/Linux?</title>
<para>
Há vários mecanismos de busca que servem para localizar documentações
relacionadas ao Debian.
</para>
<itemizedlist>
<listitem>
<para>
<ulink url="http://www.br.debian.org/search">Site de pesquisa WWW do
Debian</ulink>.
</para>
</listitem>
<listitem>
<para>
<ulink url="http://www.dejanews.com/">Serviço de pesquisa de news
DejaNews</ulink>.  Para descobrir que experiências as pessoas tiveram para
achar drivers para controladores Western Digital, experimente procurar por esta
frase: <literal>linux & WD</literal> Isso diz ao DejaNews para que relate
quaisquer artigos contendo ambas as strings "linux" E "WD".  (como nota: quando
um dos autores deste texto o usou, descobriu que sua placa WD (que ele posuia
há apenas 6 meses) tinha sido declarada obsoleta pela Adaptec, agora que eles
compraram a WD.  Portanto, não havia mais drivers disponíveis (bendita
Adaptec)).
</para>
</listitem>
<listitem>
<para>
Quaisquer dos mecanismos de busca na web (como o <ulink
url="http://www.altavista.com/">AltaVista</ulink> ou o <ulink
url="http://www.google.com">Google</ulink>) também podem ser usados para
procurar a Usenet (embora não sejam tão atualizados quanto o DejaNews).
</para>
<para>
Por exemplo, procurando pela string "cgi-perl" uma descrição mais detalhada
é retornada para esse pacote do que uma breve descrição em seu arquivo de
controle.
</para>
</listitem>
</itemizedlist>
</section>

<section id="buglogs"><title>Existem registros de bugs conhecidos?</title>
<para>
A distribuição Debian GNU/Linux possui um sistema de monitoramento de bugs
(bug tracking system (BTS)) que armazena detalhes de bugs relatados por
usuários e desenvolvedores.  A cada bug é dado um número, e é mantido
armazenado até que seja marcado como tendo sido resolvido.
</para>
<para>
Cópias dessas informações estão disponíveis em <ulink
url="http://www.br.debian.org/Bugs/">http://www.br.debian.org/Bugs/</ulink>.
</para>
<para>
Um servidor de mail oferece acesso a base de dados do sistema de monitoramento
de bugs via correio eletrônico.  Para adquirir as instruções, envie um
e-mail a <email>request@bugs.debian.org</email> com "help" no corpo da
mensagem.
</para>
</section>

<section id="bugreport"><title>Como posso informar sobre um bug no Debian?</title>
<para>
Se você encontrou um bug no Debian, por favor, leia as instruções para
informar um bug no Debian.  Essas instruções podem ser obtidas em um de
vários métodos:
</para>
<itemizedlist>
<listitem>
<para>
Por FTP anônimo.  Os sites espelhos do Debian possuem as instruções no
arquivo <literal>doc/bug-reporting.txt</literal>.
</para>
</listitem>
<listitem>
<para>
A partir da WWW.  Uma cópia das instruções é apresentada em <ulink
url="http://www.br.debian.org/Bugs/Reporting">http://www.br.debian.org/Bugs/Reporting</ulink>.
</para>
</listitem>
<listitem>
<para>
Em qualquer sistema Debian que tenha o pacote <systemitem
role="package">doc-debian</systemitem> instalado.  As instruções estão no
arquivo <filename>/usr/doc/debian/bug-reporting.txt</filename>.
</para>
</listitem>
</itemizedlist>
<para>
Você pode usar os pacotes <systemitem role="package">bug</systemitem> ou
<systemitem role="package">reportbug</systemitem> que irá guiá-lo através
pelo processo de registro e enviará as mensagens para o endereço apropriado,
com alguns detalhes adicionais sobre o seu sistema adicionados automaticamente.
</para>
<para>
Se você deseja enviar o relatório com um MUA, envia a mensagem para
<email>submit@bugs.debian.org</email>, onde a primeira linha contenha algo do
tipo
</para>
<screen>
Package: NomeDoPacote
</screen>
<para>
(substitua "NomeDoPacote" com o nome do pacote).  O resto da mensagem deve
conter a descrição do bug (por favor, faça isso de forma mais ou menos
detalhada), a versão do Debian que você está usando, e as versões
pertinentes daquele pacote.
</para>
<para>
Espere adquirir um conhecimento automático de seu relatório de bug.  Será
automaticamente dado a ele um número de monitoramento de bug, adicionado ao
registro de bugs e remetido a lista de discussão debian-bugs-dist.
</para>
<para>
Se alguém fosse identificar um bug que era comum a muitos programas, então,
ao invés de reportar dúzias de relatórios de bugs similares, alguém poderia
preferir enviar bugs individuais para <email>maintonly@bugs.debian.org</email>
(ao invés do endereço submit@...) para avisar somente o mantenedor do
respectivo pacote, e então enviar um relatório reduzido as listas de
discussão debian-devel e/ou de debian-bugs-dist.
</para>
<para>
Adicionalmente, lá existe um verificador de pacotes Debian, chamado <ulink
url="http://www.br.debian.org/lintian/">Lintian</ulink>, que é projetado para
verificar mecanicamente os pacotes Debian em relação a violações da
política e erros comuns de empacotamento.  Portanto, se você encontrar um bug
que aparentemente aparece em outros pacotes também, será melhor você entrar
em contato com o mantenedor do Lintian em
<email>lintian-maint@debian.org</email> de forma que uma nova verificação
para o Lintian seja escrita, ao invés de reportar um bug diretamente.  Isso
prevenirá que o bug apareça em futuras versões do pacote, ou em qualquer
outro pacote da distribuição.
</para>
<para>
Você também pode usar <email>quiet@bugs.debian.org</email>, para enviar um
relatório de bug ao BTS, sem os ter que enviar para a debian-bugs-dist ou para
o mantenedor.  Esse endereço é usado muito raramente, por exemplo, quando
você quer enviar alguma informação menor para seu relatório, que deve
somente ser adicionada ao seu registro, ou quando você quer adicionar algo ao
registro do BTS, mas já enviou isso para o mantenedor.
</para>
</section>

</chapter>

